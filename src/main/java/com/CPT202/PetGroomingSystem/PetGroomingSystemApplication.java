package com.CPT202.PetGroomingSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetGroomingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetGroomingSystemApplication.class, args);
	}

}
