package com.CPT202.PetGroomingSystem.MA.CS.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CPT202.PetGroomingSystem.MS.GF.models.Groomer;

public interface RelevantGroomerRepo extends JpaRepository<Groomer, Integer> {
    
}