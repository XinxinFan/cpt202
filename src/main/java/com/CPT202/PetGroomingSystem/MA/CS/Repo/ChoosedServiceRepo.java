package com.CPT202.PetGroomingSystem.MA.CS.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CPT202.PetGroomingSystem.MA.CS.models.MAServce;



public interface ChoosedServiceRepo extends JpaRepository<MAServce, Integer>{
    
}
