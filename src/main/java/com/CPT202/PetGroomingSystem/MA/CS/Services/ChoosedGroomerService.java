package com.CPT202.PetGroomingSystem.MA.CS.Services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MA.CS.Repo.ChoosedGroomerRepo;
import com.CPT202.PetGroomingSystem.MA.CS.Repo.ChoosedServiceRepo;
import com.CPT202.PetGroomingSystem.MA.CS.models.MAGroomer;
import org.springframework.data.domain.Sort;

@Service
public class ChoosedGroomerService {
    @Autowired
    private ChoosedGroomerRepo choosedgroomerRepo;
    @Autowired
    private ChoosedServiceRepo choosedserviceRepo;

    public List<MAGroomer> getList() { 
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        return choosedgroomerRepo.findAll(sort); 
    }


    public MAGroomer newGroomer(MAGroomer s) {
        if (Objects.equals(s.getName(), "") ) {
            return null;
        }
        return choosedgroomerRepo.save(s);
    }


    public void clearrepo(){
        if(choosedserviceRepo.findAll().isEmpty()){
            choosedgroomerRepo.deleteAll();
        }
            
        
        
    }
}
