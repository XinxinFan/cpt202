package com.CPT202.PetGroomingSystem.MA.CS.Services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MA.CS.Repo.ServiceRepo;
import com.CPT202.PetGroomingSystem.MS.US.models.Servce;

import org.springframework.data.domain.Sort;

@Service
public class MakeAppointmentService {
    @Autowired
    private ServiceRepo serviceRepo;
    public Servce newService(Servce s) {
        if (Objects.equals(s.getName(), "") || Objects.equals(s.getInfo(), "") || s.getPrice() == 0) {
            return null;
        }
        return serviceRepo.save(s);
    }

    

    public List<Servce> getList() { 
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        return serviceRepo.findAll(sort); 
    }

    
}
