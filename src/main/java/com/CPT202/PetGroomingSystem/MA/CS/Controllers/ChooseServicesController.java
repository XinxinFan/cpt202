package com.CPT202.PetGroomingSystem.MA.CS.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedServiceService;
import com.CPT202.PetGroomingSystem.MA.CS.Services.MakeAppointmentService;
import com.CPT202.PetGroomingSystem.MA.CS.models.MAServce;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@Controller
@RequestMapping("/Service")
public class ChooseServicesController extends MArootController{
    @Autowired
    private MakeAppointmentService makeappointmentService;
    @Autowired
    private ChoosedServiceService choosedServiceService;

    @GetMapping("/list")
    public String listServce(Model model){
        model.addAttribute("Services", makeappointmentService.getList());
        return "user/ListService";
    }
    
    // @PostMapping("/list")
    // public String confirmchoose(Model model){
    //     return "user/MakeAppointmentPage";
    // }
    
    @RequestMapping(value="/list",method=RequestMethod.POST)
    public String choosedservice(@RequestParam(value="Services", required=false)String Services,Model model){
        int id=1;
        // model.addAttribute("choosedservice", Services);
        if(Services==null){
            model.addAttribute("ChoosedEmptyErr", "Please choose at least one service");
            return listServce(model);
        }
        else{
            for(String sev:Services.split(",")){
                MAServce mas = new MAServce();
                mas.setName(sev);
                mas.setId(id);
                MAServce MAS = choosedServiceService.newService(mas);
                id++;
        }
            // model.addAttribute("choosedservice", choosedServiceService.getList());//Services表示选项是否选中，如果选中则返回服务的名称
           
        
    //    model.addAttribute("choosedService", choosedServiceService.getList());
    
        return backMakeAppointment(model);
        // return "user/MakeAppointmentPage";
    }
    }
        
}
