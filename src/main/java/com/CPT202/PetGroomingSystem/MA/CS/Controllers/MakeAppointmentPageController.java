package com.CPT202.PetGroomingSystem.MA.CS.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedGroomerService;
import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedServiceService;



@Controller
public class MakeAppointmentPageController extends MArootController{
    @Autowired
    private ChoosedGroomerService choosedgroomerservice;
    @Autowired
    private ChoosedServiceService choosedserviceservice;

    @GetMapping("/MakeAppointment")
    public String MakeAppointmentPage(Model model){
        choosedserviceservice.clearrepo();
        choosedgroomerservice.clearrepo();
        return backMakeAppointment(model);
    }


    
}
