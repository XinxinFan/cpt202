package com.CPT202.PetGroomingSystem.MA.CS.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.CPT202.PetGroomingSystem.MA.CS.Repo.ChoosedServiceRepo;
import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedGroomerService;
import com.CPT202.PetGroomingSystem.MA.CS.Services.RelevantGroomerService;
import com.CPT202.PetGroomingSystem.MA.CS.models.MAGroomer;
import com.CPT202.PetGroomingSystem.MS.GF.models.Groomer;

@Controller
@RequestMapping("/AvailableGroomers")
public class ChooseGroomerController extends MArootController{
    @Autowired
    private RelevantGroomerService relevantgroomerService;
    @Autowired
    private ChoosedGroomerService choosedgroomerService;
    @Autowired
    private ChoosedServiceRepo choosedserviceRepo;


    @GetMapping("/list")
    public String listGroomer(Model model){
        model.addAttribute("Groomers", relevantgroomerService.getList());
        return "user/ListRelevantGroomer";
    }




    @RequestMapping(value="/list",method=RequestMethod.POST)
    public String choosedgroomer(@RequestParam(value="Groomers", required=false)String Groomers,Model model){
        int id=1;
        if(Groomers==null){
            model.addAttribute("ChoosedEmptyErr", "Please choose at least one groomer or select ANY");
            return listGroomer(model);
        }
        else if(Groomers.equals("ANY")){
            return null;
        }
        else if(choosedserviceRepo.findAll().isEmpty()){
            model.addAttribute("OrderErr", "Please choose service first");
            return listGroomer(model);
        }
        else{
            for(String gro:Groomers.split(",")){
                MAGroomer mag = new MAGroomer();
                mag.setName(gro);
                mag.setId(id);
                MAGroomer MAG = choosedgroomerService.newGroomer(mag);
                id++;
        }
            // model.addAttribute("choosedGroomer", Groomers);
           
        
        return backMakeAppointment(model);
    }
}
}