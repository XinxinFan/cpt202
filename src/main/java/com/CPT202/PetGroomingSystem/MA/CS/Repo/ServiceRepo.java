package com.CPT202.PetGroomingSystem.MA.CS.Repo;

import com.CPT202.PetGroomingSystem.MS.US.models.Servce;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepo extends JpaRepository<Servce, Integer> {
}
