package com.CPT202.PetGroomingSystem.MA.CS.Services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MA.CS.Repo.ChoosedServiceRepo;
import com.CPT202.PetGroomingSystem.MA.CS.models.MAServce;

import org.springframework.data.domain.Sort;

@Service
public class ChoosedServiceService {
    @Autowired
    private ChoosedServiceRepo choosedserviceRepo;

    public MAServce newService(MAServce s) {
        if (Objects.equals(s.getName(), "") ) {
            return null;
        }
        return choosedserviceRepo.save(s);
    }

    

    public List<MAServce> getList() { 
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        return choosedserviceRepo.findAll(sort); 
    }

    public void clearrepo(){
        choosedserviceRepo.deleteAll();
    }
}
