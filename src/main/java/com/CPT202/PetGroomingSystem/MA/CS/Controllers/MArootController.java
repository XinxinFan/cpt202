package com.CPT202.PetGroomingSystem.MA.CS.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedGroomerService;
import com.CPT202.PetGroomingSystem.MA.CS.Services.ChoosedServiceService;
import com.CPT202.PetGroomingSystem.MA.CS.Services.MakeAppointmentService;




@Controller
public class MArootController {
    @Autowired
    private ChoosedGroomerService choosedgroomerservice;
    @Autowired
    private ChoosedServiceService choosedserviceservice;
    

    public String backMakeAppointment(Model model){
        model.addAttribute("choosedservice", choosedserviceservice.getList());
        model.addAttribute("choosedGroomer", choosedgroomerservice.getList());
        choosedgroomerservice.clearrepo();
        
        
        
        return "user/MakeAppointmentPage";
    }
}
