package com.CPT202.PetGroomingSystem.MA.CS.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MA.CS.Repo.RelevantGroomerRepo;
import com.CPT202.PetGroomingSystem.MS.GF.models.Groomer;
import org.springframework.data.domain.Sort;

@Service
public class RelevantGroomerService {
    @Autowired
    private RelevantGroomerRepo relevantgroomerRepo;

    public List<Groomer> getList() { 
        Sort sort = Sort.by(Sort.Direction.ASC, "GroomerName");
        return relevantgroomerRepo.findAll(sort); 
    }

}
