package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.HomePage.Controller.rootController;
import com.CPT202.PetGroomingSystem.MS.US.Services.ServceService;
import com.CPT202.PetGroomingSystem.MS.US.models.Servce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/Service")
public class ServceController extends rootController {
    @Autowired
    private ServceService servceService;

    @PostMapping("/add")
    public String confirmNewServce(@ModelAttribute("servce") Servce s, Model model) {
        Servce Sv = servceService.newService(s);
        if (Sv == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return addServce(model);
        }
        return backMaintainPage(model);
    }
    @GetMapping("/add")
    public String addServce(Model model) {
        model.addAttribute("servce", new Servce());
        return "admin/AddServce";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editServce(@PathVariable String id, Model model) {
        Servce oldSv = servceService.findById(Integer.valueOf(id));
        model.addAttribute("oldSv", oldSv);
        return "admin/EditServce";
    }

    @PostMapping("/edit")
    public String updateServce(@ModelAttribute("oldSv") Servce newSv, Model model) {
        Servce Sv = servceService.newService(newSv);
        if (Sv == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return "admin/EditServce";
        }
        return backMaintainPage(model);
    }
}
