package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.HomePage.Controller.rootController;
import com.CPT202.PetGroomingSystem.MS.US.Services.DiscountService;
import com.CPT202.PetGroomingSystem.MS.US.models.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/Discount")
public class DiscountController extends rootController {
    @Autowired
    private DiscountService discountService;

    @PostMapping("/add")
    public String confirmNewDiscount(@ModelAttribute("discount") Discount d, Model model) {
        Discount Dis = discountService.newDiscount(d);
        if (Dis == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return addDiscount(model);
        }
        return backMaintainPage(model);
    }

    @GetMapping("/add")
    public String addDiscount(Model model) {
        model.addAttribute("discount", new Discount());
        return "admin/AddDiscount";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editDiscount(@PathVariable String id, Model model) {
        Discount oldDis = discountService.findById(Integer.valueOf(id));
        model.addAttribute("oldDis", oldDis);
        return "admin/EditDiscount";
    }

    @PostMapping("/edit")
    public String updateDiscount(@ModelAttribute("oldDis") Discount newDis, Model model) {
        Discount Dis = discountService.newDiscount(newDis);
        if (Dis == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return "admin/EditDiscount";
        }
        return backMaintainPage(model);
    }

}
