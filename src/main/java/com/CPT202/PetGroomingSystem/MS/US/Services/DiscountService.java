package com.CPT202.PetGroomingSystem.MS.US.Services;

import com.CPT202.PetGroomingSystem.MS.US.Controllers.DiscountController;
import com.CPT202.PetGroomingSystem.MS.US.Repo.DiscountRepo;
import com.CPT202.PetGroomingSystem.MS.US.models.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Spliterator;


@Service
public class DiscountService {
    @Autowired
    private DiscountRepo discountRepo;

    public Discount newDiscount(Discount d) {
        if (Objects.equals(d.getCond(), "") || Objects.equals(d.getInfo(), "") || Objects.equals(d.getTimelmt(), ""))
            return null;
        return discountRepo.save(d);
    }

    public List<Discount> getList() {
        return discountRepo.findAll();
    }
    public List<Discount> getDescList() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        return discountRepo.findAll(sort);
    }

    public void delDiscount(Discount d) {
        discountRepo.delete(d);
    }
    public Discount findById(int id) {
        List<Discount> DisList = discountRepo.findAll();
        for (Discount i : DisList) {
            if (i.getId() == id) return i;
        }
        return null;
    }
}
