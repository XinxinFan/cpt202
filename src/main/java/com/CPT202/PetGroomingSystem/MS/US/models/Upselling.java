package com.CPT202.PetGroomingSystem.MS.US.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Upselling {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String info;
    private String cond;
    private String timelmt;
    private String cont;
    private float price;

    public Upselling() {}
    public Upselling(int id, String info, String cond, String cont, float price, String timelmt) {
        this.id = id;
        this.info = info;
        this.cond = cond;
        this.cont = cont;
        this.price = price;
        this.timelmt = timelmt;
    }

    public float getPrice() { return price; }

    public void setPrice(float price) { this.price = price; }

    public String getCont() { return this.cont; }
    public void setCont(String cont) { this.cont = cont; }
    public int getId() { return this.id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimelmt() {
        return timelmt;
    }

    public String getCond() {
        return cond;
    }

    public String getInfo() {
        return info;
    }

    public void setCond(String cond) {
        this.cond = cond;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setTimelmt(String timelmt) {
        this.timelmt = timelmt;
    }
}
