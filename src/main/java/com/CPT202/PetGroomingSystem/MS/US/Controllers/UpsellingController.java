package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.HomePage.Controller.rootController;
import com.CPT202.PetGroomingSystem.MS.US.Services.UpsellingService;
import com.CPT202.PetGroomingSystem.MS.US.models.Upselling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/Upsell")
public class UpsellingController extends rootController {
    @Autowired
    private UpsellingService upsellingService;

    @GetMapping("/add")
    public String addUpsell(Model model) {
        model.addAttribute("upsell", new Upselling());
        return "admin/AddUpselling";
    }

    @PostMapping("/add")
    public String confirmNewUpsell(@ModelAttribute("upsell") Upselling s, Model model) {
        Upselling Up = upsellingService.newUpselling(s);
        if (Up == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return addUpsell(model);
        }
        return backMaintainPage(model);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editUpsell(@PathVariable String id, Model model) {
        Upselling oldUp = upsellingService.findById(Integer.valueOf(id));
        model.addAttribute("oldUp", oldUp);
        return "admin/EditUpselling";
    }

    @PostMapping("edit")
    public String updateUpsell(@ModelAttribute("oldUp") Upselling newUp, Model model) {
        Upselling Up = upsellingService.newUpselling(newUp);
        if (Up == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return "admin/EditUpselling";
        }
        return backMaintainPage(model);
    }
}
