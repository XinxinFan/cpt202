package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.HomePage.Controller.rootController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MaintainPageController extends rootController {
    @GetMapping("/MaintainServicesPage")
    public String maintainPage(Model model) {
        return backMaintainPage(model);
    }
}
