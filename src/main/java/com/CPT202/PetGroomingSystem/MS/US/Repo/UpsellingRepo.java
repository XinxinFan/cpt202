package com.CPT202.PetGroomingSystem.MS.US.Repo;

import com.CPT202.PetGroomingSystem.MS.US.models.Upselling;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UpsellingRepo extends JpaRepository<Upselling, Integer> {
}
