package com.CPT202.PetGroomingSystem.MS.US.Services;

import com.CPT202.PetGroomingSystem.MS.US.Repo.ServceRepo;
import com.CPT202.PetGroomingSystem.MS.US.models.Servce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ServceService {
    @Autowired
    private ServceRepo servceRepo;

    public Servce newService(Servce s) {
        if (Objects.equals(s.getName(), "") || Objects.equals(s.getInfo(), "") || s.getPrice() == 0) {
            return null;
        }
        return servceRepo.save(s);
    }

    public List<Servce> getList() { return servceRepo.findAll(); }
    public List<Servce> getDescList() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        return servceRepo.findAll(sort);
    }
    public void delServce(Servce s) {
        servceRepo.delete(s);
    }
    public Servce findById(int id) {
        List<Servce> SvList = servceRepo.findAll();
        for (Servce i : SvList) {
            if (i.getId() == id) return i;
        }
        return null;
    }
}
