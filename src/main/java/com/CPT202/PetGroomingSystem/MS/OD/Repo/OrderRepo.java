package com.CPT202.PetGroomingSystem.MS.OD.Repo;
import org.springframework.data.jpa.repository.JpaRepository;

import com.CPT202.PetGroomingSystem.MS.OD.models.OrderModel;


public interface OrderRepo extends JpaRepository<OrderModel, Integer> {
  

}

