package com.CPT202.PetGroomingSystem.MS.OD.Controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminPageController {

    //http://localhost:8080/AdminHomePage
    @GetMapping("/AdminHomePage")
    public String home(){
        return "/admin/AdminHomePage";
    }
    
}

