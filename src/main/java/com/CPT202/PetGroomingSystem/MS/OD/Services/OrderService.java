package com.CPT202.PetGroomingSystem.MS.OD.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MS.OD.models.OrderModel;
import com.CPT202.PetGroomingSystem.MS.OD.Repo.OrderRepo;

import org.springframework.data.domain.Sort;

import java.util.List;



@Service
public class OrderService {
    @Autowired
    private OrderRepo orderRepo;

    public OrderModel newOrder(OrderModel order) {
        return orderRepo.save(order);
    }

    //get order list by order status
    public List<OrderModel> getOrderList(String status) {
        //status==All, show all orders
        if ("All".equals(status)) {
            return orderRepo.findAll();
        }
        OrderModel or = new OrderModel();//create a new OrderModel object or
        or.setOrderstatus(status);//Set its status attribute to the passed in order status parameter
        Example<OrderModel> example = Example.of(or);//Create an Example object using the or object as a parameter to perform query operations
        return orderRepo.findAll(example);//Call the "findAll" method of the orderRepo object, pass in the Example object as a parameter, and return a list of orders that meet the query criteria
    }

    //sort by booktime  未成功
    public List<OrderModel> getList() {
    Sort sort = Sort.by(Sort.Direction.DESC, "booktime");
        return orderRepo.findAll(sort);
    }


    //update order status
    public void updateStatus(int id, String status) {
        //search order by id
        OrderModel one = orderRepo.getReferenceById(id); 
        //Set the status of the order to the passed in status parameter
        one.setOrderstatus(status);
        orderRepo.save(one);//save to database
    }

    //cancel uncompleted order
    public void cancel(int id) {
        orderRepo.deleteById(id);
    }
}
