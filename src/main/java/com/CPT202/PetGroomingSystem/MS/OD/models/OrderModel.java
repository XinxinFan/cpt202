package com.CPT202.PetGroomingSystem.MS.OD.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class OrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String customer;
    private String groomer;
    private String pet;
    private String service;
    private Integer cost;
    private String booktime;
    private String orderstatus;

    public OrderModel() {

    }

    public OrderModel(int id, String customer, String groomer, String pet, String service, int cost, String booktime,
                      String orderstatus) {
        this.id = id;
        this.customer = customer;
        this.groomer = groomer;
        this.pet = pet;
        this.service = service;
        this.cost = cost;
        this.booktime = booktime;
        this.orderstatus = orderstatus;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getGroomer() {
        return groomer;
    }

    public void setGroomer(String groomer) {
        this.groomer = groomer;
    }

    public String getPet() {
        return pet;
    }

    public void setPet(String pet) {
        this.pet = pet;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getBooktime() {
        return booktime;
    }

    public void setBooktime(String booktime) {
        this.booktime = booktime;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }


}
