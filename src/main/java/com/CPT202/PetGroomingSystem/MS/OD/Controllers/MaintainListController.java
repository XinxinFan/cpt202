package com.CPT202.PetGroomingSystem.MS.OD.Controllers;

import com.CPT202.PetGroomingSystem.MS.OD.Services.OrderService;
import com.CPT202.PetGroomingSystem.MS.OD.models.OrderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


//Spring Anotation
//RESTful API
@Controller
@RequestMapping("/MaintainOrdersPage")
public class MaintainListController {
    @Autowired
    private OrderService orderService;

    //localhost:8080/AdminHomePage/MaintainOrdersPage
    @GetMapping("")
    public String getList(String status, Model model) {
        model.addAttribute("orderList", orderService.getOrderList(status));
        //status means the value of order status that in the database
        model.addAttribute("status", status);
        return "/admin/MaintainOrdersPage";
    }

    //localhost:8080/AdminHomePage/MaintainOrdersPage/toAdd
    @GetMapping("/toAdd")
    public String toAdd(Model model) {
        model.addAttribute("order", new OrderModel());
        return "/admin/AddListPage";
    }

    //add date to database
    ///MaintainOrdersPage/add
    @PostMapping("/add")
    public String add(@ModelAttribute("order") OrderModel order) {
        //add the new team into database
        order.setOrderstatus("Booked");
        orderService.newOrder(order);
        return "redirect:/MaintainOrdersPage";
    }

    @GetMapping("/updateStatus")
    public String updateStatus(int id, String status) {
        orderService.updateStatus(id, status);
        return "redirect:/MaintainOrdersPage";
    }


    @GetMapping("/cancel")
    public String cancel(int id) {
        orderService.cancel(id);
        return "redirect:/MaintainOrdersPage";
    }

}
