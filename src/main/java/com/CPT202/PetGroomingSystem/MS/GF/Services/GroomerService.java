package com.CPT202.PetGroomingSystem.MS.GF.Services;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.MS.GF.Repo.GroomerRepo;
import com.CPT202.PetGroomingSystem.MS.GF.models.Groomer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class GroomerService {
    
    @Autowired
    private GroomerRepo groomerRepo;


    /*
     * add groomer in database
     * @param All properties related to groomer
     */
    public Groomer newGroomer(Groomer g) {
        // if (customerRepo.existsByName(c.getCustomerName())){//not sure 
        //     Customer fake = new Customer(-8999, "fake", 9999, "fake", "fake");
        //     return fake;
        // }
        
        if (Objects.equals(g.getGroomerName(), "") || Objects.equals(g.getPhoneNumber(), "") 
        || Objects.equals(g.getEmail(), "") || Objects.equals(g.getAvailability(), ""))
            
            return null;
        
        return groomerRepo.save(g);
    }

    /*
     * Groomer list
     */
    public List<Groomer> getGroomerList(){
        return groomerRepo.findAll();
    }

    /*
     * sort by alphabetical order
     */
    // public List<Groomer> getSortByName(){
    // // public void getSortByName(){
    //     // List.sort(Comparator.comparing(Groomer::getGroomerName));

    //     return getGroomerList();

    // }
        

    public Groomer findById(int id) {
        List<Groomer> GroomerList = groomerRepo.findAll();
        for (Groomer i : GroomerList) {
            if (i.getId() == id) return i;
        }
        return null;
    }
   //@Override
    
    public Groomer getGroomerById(int id) {
        return groomerRepo.findById(id).orElseThrow(GroomerNotFoundException::new);
    }

    public class GroomerNotFoundException extends RuntimeException {
    }

    public void delGroomer(Groomer g) {
        groomerRepo.delete(g);
    }



}
