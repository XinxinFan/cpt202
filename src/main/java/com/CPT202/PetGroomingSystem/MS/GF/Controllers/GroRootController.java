package com.CPT202.PetGroomingSystem.MS.GF.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.CPT202.PetGroomingSystem.MS.GF.Services.GroomerService;

@Controller
public class GroRootController {
    @Autowired
    private GroomerService groomerService;

    /*
     * return Maintain Customer Page
     */
    public String returnMaintainGroomerPage(Model model){
        model.addAttribute("GroomerList", groomerService.getGroomerList());
        return "admin/MaintainGroomerPage";

    }
}
