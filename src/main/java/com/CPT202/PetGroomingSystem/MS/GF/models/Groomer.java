package com.CPT202.PetGroomingSystem.MS.GF.models;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Groomer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    //unique name
    //@Column(name="CustomerName", unique = true)
    private String groomerName;
    private int phoneNumber;
    private String email;
    private String availability; //fake type for availability
    private int ranking;
    private String GroomerService;

    public Groomer(int id, String groomerName, int phoneNumber, String email, String availability, int ranking,
            String GroomerService) {
        this.id = id;
        this.groomerName = groomerName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.availability = availability;
        this.ranking = ranking;
        this.GroomerService = GroomerService;
    }

    public Groomer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getGroomerName() {
        return groomerName;
    }
    public void setGroomerName(String groomerName) {
        this.groomerName = groomerName;
    }
    public int getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAvailability() {
        return availability;
    }
    public void setAvailability(String availability) {
        this.availability = availability;
    }
    public int getRanking() {
        return ranking;
    }
    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
    public String getGroomerService() {
        return GroomerService;
    }
    public void setGroomerService(String GroomerService) {
        this.GroomerService = GroomerService;
    }


    
    // public Groomer(int id, String groomerName, int phoneNumber, String email, String availability) {
    //     this.id = id;
    //     GroomerName = groomerName;
    //     this.phoneNumber = phoneNumber;
    //     this.email = email;
    //     this.availability = availability;
    // }

    // public int getGroomerId() {
    //     return GroomerId;
    // }
    // public void setGroomerId(int groomerId) {
    //     GroomerId = groomerId;
    // }
    

    

    
    
}
