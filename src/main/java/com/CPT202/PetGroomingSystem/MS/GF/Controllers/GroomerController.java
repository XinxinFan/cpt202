
package com.CPT202.PetGroomingSystem.MS.GF.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.CPT202.PetGroomingSystem.MS.GF.Services.GroomerService;
import com.CPT202.PetGroomingSystem.MS.GF.models.Groomer;



@Controller
@RequestMapping("/Groomer")
public class GroomerController extends GroRootController{
//
    @Autowired
    private GroomerService groomerService;

    @GetMapping("/add")
    public String addGroomer(Model model){
        model.addAttribute("groomer", new Groomer());
        return "admin/AddGroomer";
    }

     @PostMapping("/add")
     public String confirmNewCustomer(@ModelAttribute("groomer") Groomer g, Model model){
        Groomer groomer = groomerService.newGroomer(g);
        if(groomer == null){
             model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                     "correct information");
             return addGroomer(model);
        }
        // if(customer.getCustomerName()=="fake"){
        //     model.addAttribute("DuplicateNameErr", 
        //     "Duplicate customer name. Please enter another name");
        //     return addCustomer(model);
        // }

      return returnMaintainGroomerPage(model);

}

// @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
// public String editGroomer(@PathVariable String id, Model model) {
//     Groomer oldGroomer = groomerService.findById(Integer.valueOf(id));
//     model.addAttribute("oldGroomer", oldGroomer);
//     return "admin/GroomerFile/EditGroomer";
// }

// @PostMapping("/edit")
// public String updateGroomer(@ModelAttribute("oldGroomer") Groomer newInfo, Model model) {
//     Groomer updatedInfo = groomerService.newGroomer(newInfo);
//     if (updatedInfo == null) {
//         model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
//                 "correct information");
//         return "admin/EditDiscount";
//     }
//     return returnMaintainGroomerPage(model);
// }


}



