package com.CPT202.PetGroomingSystem.MS.GF.Controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
//import com.CPT202.PetGroomingSystem.CustomerFile.Controller.CusRootController;

@Controller
public class MaintainGroPageContr extends GroRootController{
    @GetMapping("/MaintainGroomerPage")
    public String maintainGroPage(Model model) {
        return returnMaintainGroomerPage(model);
    }
}
