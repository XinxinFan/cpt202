package com.CPT202.PetGroomingSystem.MS.CF.Controllers;

import com.CPT202.PetGroomingSystem.HomePage.Controller.rootController;
import com.CPT202.PetGroomingSystem.MS.CF.Services.CustomerService;
import com.CPT202.PetGroomingSystem.MS.CF.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller

//@RequestMapping(value = "/Customer") 改进一
@RequestMapping(value = "/MaintainCustomersPage")
public class CustomerController extends rootController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("")
    public String getList(String status, Model model) {

        model.addAttribute("CustomerList", customerService.getDescList());
        return "admin/MaintainCustomersPage";
    }

    @GetMapping("/add")
    public String addCustomer(Model model) {
        model.addAttribute("customer", new Customer());
        return "admin/AddCustomer";
    }

    @PostMapping("/add")
    public String confirmNewCustomer(@ModelAttribute("customer") Customer c, Model model) {
        Customer Cus = customerService.newCustomer(c);
        if (Cus == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return addCustomer(model);
        }
        return "redirect:/MaintainCustomersPage";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editCustomer(@PathVariable String id, Model model) {
        Customer oldCustomer = customerService.findById(Integer.valueOf(id));
        model.addAttribute("oldCustomer", oldCustomer);
        return "admin/EditCustomer";
    }

    @PostMapping("/edit")
    public String updateCustomer(@ModelAttribute("oldCustomer") Customer newCustomer, Model model) {
        Customer Cus = customerService.newCustomer(newCustomer);
        if (Cus == null) {
            model.addAttribute("FieldEmptyErr", "Required field is empty, please fill in \n" +
                    "correct information");
            return "admin/EditCustomer";
        }
       return "redirect:/MaintainCustomersPage";
    }
    @GetMapping("/delete")
    public String delCustomer(Customer c) {
        customerService.delCustomer(c);
        return "redirect:/MaintainOrdersPage";
    }

}
