package com.CPT202.PetGroomingSystem.MS.CF.Services;

import com.CPT202.PetGroomingSystem.MS.CF.Repo.CustomerRepo;
import com.CPT202.PetGroomingSystem.MS.CF.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepo customerRepo;

    public Customer newCustomer(Customer c) {
        if (Objects.equals(c.getName(), "") || Objects.equals(c.getAddr(), "") || Objects.equals(c.getTel(), "") || Objects.equals(c.getEmail(), ""))
            return null;
        return customerRepo.save(c);
    }

    public List<Customer> getList() {
        return customerRepo.findAll();
    }
    public List<Customer> getDescList() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        return customerRepo.findAll(sort);
    }
    public void delCustomer(Customer c)
    {
        customerRepo.delete(c);
    }

    public Customer findById(int id) {
        List<Customer> CustomerList = customerRepo.findAll();
        for (Customer i : CustomerList) {
            if (i.getId() == id) return i;
        }
        return null;
    }
}
