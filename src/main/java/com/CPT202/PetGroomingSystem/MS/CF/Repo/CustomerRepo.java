package com.CPT202.PetGroomingSystem.MS.CF.Repo;

import com.CPT202.PetGroomingSystem.MS.CF.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {

}
