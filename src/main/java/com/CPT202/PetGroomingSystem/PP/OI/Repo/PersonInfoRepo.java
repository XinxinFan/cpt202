package com.CPT202.PetGroomingSystem.PP.OI.Repo;

import com.CPT202.PetGroomingSystem.PP.OI.models.PersonInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonInfoRepo extends JpaRepository<PersonInfo, Integer> {
}
