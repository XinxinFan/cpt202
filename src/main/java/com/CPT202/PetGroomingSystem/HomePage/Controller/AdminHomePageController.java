package com.CPT202.PetGroomingSystem.HomePage.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminHomePageController {
    @GetMapping("/Admin")
    public String getAdminHome (Model model) {
        return "admin/AdminHomePage";
    }
}
