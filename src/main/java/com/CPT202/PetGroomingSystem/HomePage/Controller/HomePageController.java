package com.CPT202.PetGroomingSystem.HomePage.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomePageController extends rootController {

    @GetMapping("/")
    public String getHomePage(Model model) {
        return loadHomePage(model);
    }
}
